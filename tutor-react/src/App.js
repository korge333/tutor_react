import './styles/styles.css';
import Title from './components/Title'
import Objective from './components/Objective';
import KeyKnowledge from './components/KeyKnowledge'
import Tasks from './components/Tasks';
import Autoevaluation from './components/Autoevaluation'
import Recursos from './components/Recursos'

function App() {
  return (
    <div className="app">
      <Title/>
      <Objective/>
      <KeyKnowledge/>
      <Tasks/>
      <Recursos/>
      <Autoevaluation/>
    </div>
  );
}

export default App;
