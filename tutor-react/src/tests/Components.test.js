import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import {render} from '@testing-library/react'
import Title from '../components/Title'
import Objective from '../components/Objective';
import KeyKnowledge from '../components/KeyKnowledge'
import Tasks from '../components/Tasks';
import Autoevaluation from '../components/Autoevaluation'
import Recursos from '../components/Recursos'


test('renders Title',()=>{
    const component = render(<Title/>)
    component.getByText('Command Line')
})
test('renders Objective',()=>{
    const component = render(<Objective/>)
    component.getByText('Objetivos:')
})
test('renders KeyKnowledge',()=>{
    const component = render(<KeyKnowledge/>)
    component.getByText('Conocimientos clave')
})
test('renders Task',()=>{
    const component = render(<Tasks/>)
    component.getByText('CDM Ejercicios Básicos Command Line')
})
test('renders Recursos',()=>{
    const component = render(<Recursos/>)
    component.getByText('Recursos extra')
})
test('renders Autoevaluation',()=>{
    const component = render(<Autoevaluation/>)
    component.getByText('Autoevaluación')
})