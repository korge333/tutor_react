import React from 'react'

function Recurso(props) {
    return (
        <div>
            <a href={props.linked}>{props.name}</a>
        </div>
    )
}

export default Recurso
