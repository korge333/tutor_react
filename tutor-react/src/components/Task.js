import React from 'react'

function Task(props) {
    return (
        <div>
            <li><a href={props.linked}>{props.name}</a></li>
        </div>
    )
}

export default Task
