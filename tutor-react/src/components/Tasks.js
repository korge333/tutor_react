import React from 'react'
import Task from './Task'

function Tasks() {
    return (
        <div>
            <h2>CDM Ejercicios Básicos Command Line</h2>
            <Task name ='Hello world' linked='https://cmdchallenge.com/#/hello_world'/>
            <Task name ='Print the current working directory' linked='https://cmdchallenge.com/#/current_working_directory'/>
            <Task name ='List names of all the files in the current directory' linked='https://cmdchallenge.com/#/list_files'/>
            <Task name ='Print the contents' linked='https://cmdchallenge.com/#/print_file_contents'/>
            <Task name ='Last lines' linked='https://cmdchallenge.com/#/last_lines'/>
            <Task name ='Create an empty file' linked='https://cmdchallenge.com/#/create_file'/>
            <Task name ='Create a directory' linked='https://cmdchallenge.com/#/create_directory'/>
            <Task name ='Copy the file' linked='https://cmdchallenge.com/#/copy_file'/>
            <Task name ='Move the file' linked='https://cmdchallenge.com/#/move_file'/>
            <Task name ='Create a symbolic link' linked='https://cmdchallenge.com/#/create_symlink'/>
            <Task name ='Delete all of the files' linked='https://cmdchallenge.com/#/delete_files'/>
            <Task name ='Remove all files with extension' linked='https://cmdchallenge.com/#/remove_files_with_extension'/>
            <Task name ='Find a String in a file' linked='https://cmdchallenge.com/#/find_string_in_a_file'/>
            <Task name ='Search for files containing strings' linked='https://cmdchallenge.com/#/search_for_files_containing_string'/>
            <Task name ='Search for files by extension' linked='https://cmdchallenge.com/#/search_for_files_by_extension'/>
            <Task name ='Search for strings  in files recursive' linked='https://cmdchallenge.com/#/search_for_string_in_files_recursive'/>
        </div>
    )
}

export default Tasks
