import React from 'react'

function KeyKnowledge() {
    return (
        <div>
            <h2>Conocimientos clave</h2>
                <li>¿Qué es la terminal?</li>
                <li>Comandos básicos de navegación</li>
                <li>Creación, visión y eliminación de ficheros y carpetas</li>
                <li>Búsquedas</li>
                <li>Concepto de entrada y salida</li>
                <li>Encadenar comandos</li>
                <li>Obtener ayuda</li>
            <h3>Lectura inicial sugerida:</h3>
            <a href="https://es.wikibooks.org/wiki/Introducci%C3%B3n_a_Linux/B%C3%BAsqueda_y_consulta_de_documentaci%C3%B3n">
            Búsqueda y consulta de documentación
            </a>
        </div>
    )
}

export default KeyKnowledge
