import React from 'react'
import Checkbox from './Checkbox'

function Autoevaluation() {
    return (
        <div>
            <h2>Autoevaluación</h2>
            <Checkbox name='cd'/>
            <Checkbox name='ls'/>
            <Checkbox name='pwd'/>
            <Checkbox name='touch'/>
            <Checkbox name='grep'/>
            <Checkbox name='more/less/cat'/>
            <Checkbox name='rm'/>
            <Checkbox name='rmdir'/>
            <Checkbox name='mkdir'/>
            <Checkbox name='tail'/>
            <Checkbox name='mv'/>
            <Checkbox name='<'/>
            <Checkbox name='>'/>
            <Checkbox name='man'/>
            <Checkbox name='echo'/>
            <Checkbox name='|'/>

        </div>
    )
}

export default Autoevaluation
