import React from 'react'
import Recurso from './Recurso'

function Recursos() {
    return (
        <div>
            <h2>Recursos extra</h2>
            <Recurso name='Command Line Challenge' linked='https://cmdchallenge.com/'/>
            <Recurso name='Learning the shell' linked='http://linuxcommand.org/lc3_learning_the_shell.php'/>
            <Recurso name='Writing shell scripts' linked='http://linuxcommand.org/lc3_writing_shell_scripts.php'/>
            <Recurso name='Command line Mystery' linked='https://github.com/veltman/clmystery'/>
            <Recurso name='Línea de comandos en castellano' linked='https://fortinux.gitbooks.io/humble_tips/content/usando_la_linea_de_comandos'/>
            <Recurso name='KataCoda: Linux Commands' linked='https://www.katacoda.com/cybershaolin/courses/intro-to-linux/linux-commands-training_kwoon'/>
        </div>
    )
}

export default Recursos
